- 09:30-10:15 card game
- (help with databricks account)
- 10:30-11:15 cloud computing
- 11:30-12:15 scaling / concurrency
- (12:15-13:15 lunch)
- 13:15-14:00 spark distributions

- 14:15-15:00 docker hub
- 15:15-16:00 databases
- 16:00-16:30 wrap up & discussion
