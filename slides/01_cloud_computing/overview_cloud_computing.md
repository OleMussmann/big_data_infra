## Cloud Computing & Security

### pros & cons

+ reliability (backup/sync)
+ security
+ setup
+ cost (pay-as-you-go, licenses, etc.)
+ ease of use (management, maintenanc
+ ubiquity (availability, accessibility, sharing)
+ agility (deployment/scaling/prototyping)

- reliability (out of your control)
- security (no physical access)
- ease of use
- dependency (ToS, service)
- internet connection (reliability, bandwidth, ping)
- vendor lock-in (migration)
- lack of standards
- privacy (warrants, encryption)
- control (customization)
- transparency, debugging

### Deployment
- private
- public
- hybrid
- community

### Flavours
- cloud ("someone else's computer")
- fog (node)
- edge (device)
- dew - storage: dropbox, software: {app,play}store, platform: github

### Service Models: ?aaS
everything as a service
- IaaS infrastructure - hosting
  - Compute, (Block) Storage, Networking
  - Your own, shielded, data center
  - Your own responsibility
- IaaS+ infrastructure
  - Kubernetes
  - Swarm
  - Containers
- Paas platform - os, dev env, db, webserver (Daas desktop)
  - Many different types of services for storage, code development, analytics
  - File Storage (e.g. S3)
  - Relational Databases
  - Document Database (e.g. MongoDB)
  - Analytics Clusters (e.g. Spark, Presto based)
  - Managed Jupyter notebooks with GPU an Machine Learning accelerators (TPU)
  - Data Warehouse, e.g. Redshift, BigQuery. Query petabytes of data using SQL
  - Development Pipelines, code repositories
- Saas software - office 365 (Gaas games)
  - Fully managed standard tools
  - Office 365, Google GSuite
  - SalesForce, Slack, WhatsApp
- Faas function - "serverless"

### Cost Control and Governance
- Cloud is pay per use
- Reserved resources for discount prices
  - Buy for 1 or three year and get e.g. 70% discount
- Spot images for market prices, a good way to get many resources for what you are willing to pay
  - Requires jobs that can be preempted

### Services and Responsibilities
STACK IMAGE FROM PETER!
- Shared Responsibility model
- Cloud Service Provider starts at the bottom
- Cloud Service Client decides where to take over
- Higher in the stack means less responsibility for the CSC but more trust to be put in the CSP

### Security
- The CS Client is always accountable for any data breach or loss
- CSP responsible for the security of the cloud
- Security is delegated via SLA’s and Contracts
- CSP needs to be certified for security and privacy
  - <List of certifications from Peter>
- Security of the cloud is very high
  - All controls available for
    - Monitoring, Logging, Detection, Configuration management, Automatic Governance and rule enforcement
  - Encryption in transit and at rest, everything
    - Allows Crypto Shredding
  - Encryption Key Management Services
    - Cloud managed
    - Client managed
    - Cloud owned keys
    - Client owned keys

#### Attack Vectors
- client (hardware: keyloggers, software: virus, wetware: social engineering / human errors)
- connection (wifi, lan, man-in-the-middle, ddos)
- insiders @ vendor
- server layers (hardware, virtualization, os, software, etc.)
- VM neighbors
- law enforcement

#### Counter Measures
- deterrent (... or else!)
- prevention
  - encryption
  - good passwords, good password policies
  - multi-factor authentication
- (detective)
- (corrective)

### Vendors
- AWS
- Google
- Azure
- hosting
- paperspace
INTEGRATE IN ?AAS
