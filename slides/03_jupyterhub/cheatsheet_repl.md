# Solutions
<details><summary>Yes, I'm cheating</summary>
<p>

### Start python3 console

```
python3
```

### Print `hello world`

```
print('hello world')
```

### Assign integer to `x`, print `x`

```
x=5
x
```

### Assign different integer to `x`, print `x`

```
x=7
x
```

</p>
</details>
