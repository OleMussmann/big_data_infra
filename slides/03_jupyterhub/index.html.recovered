<!doctype html>
<html lang="en">

  <head>
    <meta charset="utf-8">

    <title>JupyterHub Presentation</title>

    <meta name="description" content="A presentation about JupyterHub.">
    <meta name="author" content="Ole Mussmann">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="reveal.js/3.8.0/reveal.js/css/reset.css">
    <link rel="stylesheet" href="reveal.js/3.8.0/reveal.js/css/reveal.css">
    <link rel="stylesheet" href="reveal.js/3.8.0/reveal.js/css/theme/black.css" id="theme">

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="reveal.js/3.8.0/reveal.js/lib/css/zenburn.css">

    <!-- tweak colors and minor details -->
    <link rel="stylesheet" href="reveal.js/custom_css/eurostat_3.8.0.css" id="theme">

    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? 'reveal.js/3.8.0/reveal.js/css/print/pdf.css' : 'reveal.js/3.8.0/reveal.js/css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>

    <link rel="shortcut icon" type="image/png" href="./files/favicon.ico"/>

    <!--[if lt IE 9]>
      <script src="lib/js/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="reveal">
      <style type="text/css" media="screen">
        section {margin-top: 10vh;} /* for header */
      </style>

      <div id="hidden" style="display: none;">
        <div id="decoration">
          <div id="top_bar" style="background-color: #3465a4; position: absolute; top: 0; left: 0; margin: 0; padding: 0; z-index: 3; height: 10vh; width: 100vw;">
          </div>
          <img id="logo" style="position: absolute; top: 10vh; left: 50%; width: 18vh; padding: 0; margin-left: -9vh; margin-top: -7.7vh; z-index: 100;" src="./files/logo_symmetric.png"/>
          <div style="position: absolute; bottom: 0; margin: 0; padding-bottom: .5vh; z-index: 3; width: 100vw; text-align: center;">
            <span id="footer" style="background-color: #3465a4; color: white; text-align: center; font-size: smaller;">
              &nbsp;Eurostat&nbsp;
            </span>
          </div>
        </div>
      </div>

      <!-- Any section element inside of this container is displayed as a slide -->
      <div class="slides">

        <section data-state="dim" data-background="./files/jupiter-1700974_1920.jpg" style="margin-top: 0;">
          <!--Image by <a href="https://pixabay.com/users/Andrew-Art-2005827/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1700974">Ondřej Šponiar</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1700974">Pixabay</a>-->
          <h2>JupyterHub</h2>
              <h3>reproducible development environments</h3>
              <h5><br><a href="mailto:bo.mussmann@cbs.nl">Bjoern Ole Mussmann</a><br>Barry Schouten, Rob Warmerdam <br>CBS / Statistics Netherlands</h5>
        </section>

        <section data-state="dim" data-background="./files/jupiter-1700974_1920.jpg" style="margin-top: 0;">
          - "deploying a containerized jupyterhub server with docker"
            - jupyterhub
              - jupyter
              - jupyterlab
              - jupyterhub
            - docker
              - container
            - authentication
              - oauth
              - ldap
              - PAM
        </section>

        <section data-background="./files/docker-logo.svg" data-background-size="contain" data-background-color="rgb(200, 200, 200)" style="margin-top: 0;">
          <div style="height: 50vh;">
            <p style="position: absolute; width: 100%; bottom: 0; text-align: center; font-size: x-small;">
              <a href="https://training.play-with-docker.com/images/docker-logo.svg" style="color: black;">https://training.play-with-docker.com/images/docker-logo.svg</a>
            </p>
          </div>
        </section>

        <section data-background="./files/business-1845350_1920.jpg" data-background-color="rgb(150, 150, 150)" data-background-opacity=".3" style="margin-top: 0;">
          <!--Image by <a href="https://pixabay.com/users/Pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1845350">Pexels</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1845350">Pixabay</a>-->
          <h3>Docker vs. Virtual Machines</h3>
          <img style="height: 40vh;" data-src="./files/docker-containerized-and-vm-transparent-bg.png"></img>
          <p style="position: absolute; width: 100%; bottom: 0; text-align: center; font-size: x-small;">
            <a href="https://www.docker.com/resources/what-container" style="color: black;">https://www.docker.com/resources/what-container</a>
          </p>
        </section>

        <section data-background="./files/business-1845350_1920.jpg" data-background-color="rgb(150, 150, 150)" data-background-opacity=".3" style="margin-top: 0;">
          <!--Image by <a href="https://pixabay.com/users/Pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1845350">Pexels</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1845350">Pixabay</a>-->
          <h3>Docker host</h3>
          <img style="height: 40vh;" data-src="./files/docker-kernel.png"></img>
          <p style="position: absolute; width: 100%; bottom: 0; text-align: center; font-size: x-small;">
            <a href="https://www.docker.com/blog/docker-101-introduction-docker-webinar-recap/" style="color: black;">https://www.docker.com/blog/docker-101-introduction-docker-webinar-recap/</a>
          </p>
        </section>



        <section data-background="./files/business-1845350_1920.jpg" data-background-color="rgb(150, 150, 150)" data-background-opacity=".3" style="margin-top: 0;">
          <!--Image by <a href="https://pixabay.com/users/Pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1845350">Pexels</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1845350">Pixabay</a>-->
          <h3>In Practice</h3>
          <img style="height: 40vh;" data-src="./files/architecture.svg"></img>
          <p style="position: absolute; width: 100%; bottom: 0; text-align: center; font-size: x-small;">
            <a href="https://docs.docker.com/engine/docker-overview/" style="color: black;">https://docs.docker.com/engine/docker-overview/</a>
          </p>
        </section>

        <section data-state="dim" data-background="./files/jupiter-1700974_1920.jpg" style="margin-top: 0;">
          <h3>Demo</h3>
          <p style="text-align: center">Terminal</p>
          <iframe data-src="http://0.0.0.0:8000/term.html" style="width: 100%; height: 35vh; padding: 0;"></iframe>
          <p style="text-align: center"><a href="http://localhost:8080/">http://localhost:8080/</a></p>
        </section>

        <section data-state="dim" data-background="./files/CBS_Heerlen.jpeg" style="margin-top: 0;">
          <h3>What's the use for CBS?</h3>
          <ul>
            <div id="old_line" class="fragment">
              <li>"BuT iT rUnS oN mY LAptoP..."<br>
              "Back up your email, your laptop goes in production!"</li>
            <ul>
            </ul>
            </div>
            <div id="new_line" class="fragment">
            </div>
            <li class="fragment">Fast, consistent delivery</li>
            <li class="fragment"></li>
            <li class="fragment">Encapsulated environments, no dependency hell</li>
            <li class="fragment">Online or offline registry (Hub)</li>
          </ul>
        </section>

        <section data-state="dim" data-background="./files/jupiter-1700974_1920.jpg" style="margin-top: 0;">
          <h3>REPL</h3>
          <p style="text-align: center">Terminal</p>
          <iframe data-src="http://0.0.0.0:8000/term.html" style="width: 100%; height: 35vh; padding: 0;"></iframe>
          <p style="text-align: center"><a href="http://localhost:8080/">http://localhost:8080/</a></p>
        </section>

      </div>
    </div>

    <script src="reveal.js/3.8.0/reveal.js/js/reveal.js"></script>
    <script>

      // More info https://github.com/hakimel/reveal.js#configuration
      Reveal.initialize({
        controls: true,
        progress: false,
        center: true,
        hash: true,

        transition: 'slide', // none/fade/slide/convex/concave/zoom

        // More info https://github.com/hakimel/reveal.js#dependencies
        dependencies: [
          { src: 'reveal.js/3.8.0/reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
          { src: 'reveal.js/3.8.0/reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
          { src: 'reveal.js/3.8.0/reveal.js/plugin/highlight/highlight.js', async: true },
          { src: 'reveal.js/3.8.0/reveal.js/plugin/search/search.js', async: true },
          { src: 'reveal.js/3.8.0/reveal.js/plugin/zoom-js/zoom.js', async: true },
          { src: 'reveal.js/3.8.0/reveal.js/plugin/notes/notes.js', async: true }
        ]
      });
    </script>

    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script type="text/javascript">
      var decoration = $('#decoration').html();
      if ( window.location.search.match( /print-pdf/gi ) ) {
        // for pdf export
        // 3. On Reveal.js ready event, copy decoration <div> into each `.slide-background` <div>
        Reveal.addEventListener( 'ready', function( event ) {
          $('.slide-background').append(decoration);
        });
      }
      else {
        // for viewing slides
        $('div.reveal').append(decoration);
        $('#decoration').remove(); // without this the fadeIn/fadeOut below does not work. why is this necessary?
      };
      Reveal.addEventListener( 'term1', function( event ) {
        // remove top bar, so it does not cover the terminal
        $('#top_bar').fadeOut();
      });
      Reveal.addEventListener( 'slidechanged', function( event ) {
        var prev = Reveal.getPreviousSlide();
        if ( prev )
        {
          if ( prev.getAttribute('data-state') == "term1")
          {
            $('#top_bar').fadeIn();
          }
        }
      });
      Reveal.addEventListener( 'fragmentshown', function( event ) {
        if(event.fragment.id === "new_line"){
          console.log("change line");
          document.getElementById("old_line").style.setProperty("text-decoration", "line-through");
        };
      });

    </script>

  </body>
</html>
