# Nginx from Docker

## Start docker container

Use docker to start the IMAGE `nginx` in detached mode. Forward the container PORT `80` to port `8080` on the host machine. Note the `ID` of the container, the first three characters are sufficient.

Usage
```
sudo docker [OPTIONS] IMAGE

OPTIONS:
  -d                                  Detach from terminal.
  -p X:Y                              Forward container PORT Y to host PORT X.
  -v HOST_DIR:CONTAINER_DIR[:OPTIONS] Bind mount from HOST_DIR to CONTAINER_DIR, options include rw or ro
```

Check the website in your browser on port `8080`:

```
YOUR_IP:8080
```

## Stop and remove docker container

Clean up after you are done. The first three characters of the `ID` are sufficient.

Usage:
```
sudo docker stop ID
sudo docker rm ID
```

## Start nginx with your own content

In the `nginx` folder, make a file `index.html` and fill it with some content. As an example, this is the default page for nginx:

```
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

Start a new container from the IMAGE `nginx` in detached mode. Forward the container PORT `80` to port `8080` on the host machine. Bind mount the `nginx` directory on the host to `/usr/share/nginx/html` in the container. For security, mount the directory read-only(`ro`). Note the `ID` of the container, the first three characters are sufficient.

Check the website in your browser on port `8080`:

```
YOUR_IP:8080
```

## Stop and remove docker container

Clean up after you are done. The first three characters of the `ID` are sufficient.

Usage:
```
docker stop ID
docker rm ID
```

# Solutions
<details><summary>Yes, I'm cheating</summary>
<p>

### Start docker container

```
sudo docker run \
  -d \
  -p 8080:80 \
  nginx
```

### Start nginx with your own content

`$(pwd)` inserts the result of the command `pwd` (print working directory). Alternatively, you can type out the full path by hand: `/home/ubuntu/nginx`.

```
cd nginx

sudo docker run \
  -p 8080:80 \
  -v $(pwd):/usr/share/nginx/html:ro \
  nginx
```

</p>
</details>
