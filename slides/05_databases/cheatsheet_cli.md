# Commands

## grep

Find PATTERN in a FILE or data stream. PATTERN might need to be put in "quotation marks" to escape shell characters.

Usage:
```
grep PATTERN [FILE]
```

Example:
```
grep "Harry" Order_of_the_Phoenix.txt
```

## pipe

Use the output of one command as input for another.

Usage:
```
command1 | command2
```

Example:
```
ls | grep "txt"
```


## wc

Print newline, word, and byte counts.

Usage:
```
wc [FILE]
```

Example:
```
wc Order_of_the_Phoenix.txt
```

## sort

Sort lines of text files or data stream.

Usage:
```
sort [OPTIONS] [FILE]

OPTIONS:
  -r      Reverse sort.
  -n      Sort numerically.
  -k "N"  Sort via key: Nth column. Starts at 1.
  -t "X"  Use character "X" as delimiter.
```

Example:
```
sort -n -t "," -k 1 Employee_table.csv
```

## head

Output the first part of files or data stream.

Usage:
```
head [FILE]
```

Example:
```
head Order_of_the_Phoenix.txt
```

## tail

Output the last part of files or data stream.

Usage:
```
tail [FILE]
```

Example:
```
tail Order_of_the_Phoenix.txt
```
# Solutions
<details><summary>Yes, I'm cheating</summary>
<p>

### Header

```
head academy_awards.txt
```

### List Length

```
wc -l academy_awards.txt
```

### Age of Joaquin Phoenix

```
grep Phoenix academy_awards.txt
```

### Number of Women

```
grep ";F;" academy_awards.txt | wc -l
```

### Most Nominations

```
sort -t ";" -n -k 6 academy_awards.txt | tail
```
or
```
sort -t ";" -n -k 6 -r academy_awards.txt | head
```

### Most Wins

```
sort -t ";" -n -k 7 academy_awards.txt | tail
```
or
```
sort -t ";" -n -k 7 -r academy_awards.txt | head
```

</p>
</details>
