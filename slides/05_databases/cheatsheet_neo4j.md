# Docker

## Start docker container

Use docker to start the IMAGE `neo4j:4.0` in detached mode without authentication (set environment variable `NEO4J_AUTH` to `none`). Forward the container PORTs `7474` and `7687` to the same ports on the host machine. Note the `ID` of the container, the first three characters are sufficient.

Usage
```
sudo docker [OPTIONS] IMAGE

OPTIONS:
  -d         Detach from terminal.
  -p X:Y     Forward container PORT Y to host PORT X.
  --env X=Y  Set environment variable X to Y.
```

## Stop and remove docker container

Clean up after you are done. The first three characters of the `ID` are sufficient.

Usage:
```
sudo docker stop ID
sudo docker rm ID
```

# Neo4j

Visit `YOUR_IP:7474` in your browser. Change the `Connect URL` to `bolt://YOUR_IP:7687`, leave username and password empty, click connect.

Tutorials in the graph database user interface.

## Graph fundamentals

`Learn about Neo4j` ➡️ `Start Learning`

Afterwards, close the `:play concepts` sub-window.

## Interact with the database

There are two hands-on tutorials to try.

`Jump into code` ➡️ `Write Code`

### Movie Graph

`Movie Graph` ➡️ `Create a graph`

Afterwards, close the `play: movie-graph` sub-window.

### Northwind Graph

`Northwind Graph` ➡️ `Convert from RDBMS`

Afterwards, close the `play: northwind-graph` sub-window.

# Solutions
<details><summary>Yes, I'm cheating</summary>
<p>

### Start docker container

```
sudo docker run \
  -d \
  -p 7474:7474 \
  -p 7687:7687 \
  --env NEO4J_AUTH=none \
  neo4j:4.0
```

</p>
</details>
