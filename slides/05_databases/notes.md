# file

- header

- length of list
  wc -l academy_nominees.txt

- age of Joaquin Phoenix
  grep Phoenix academy_nominees.txt

- number of women in the list
  grep ";F;" academy_nominees.txt | wc -l

- most nominations
  sort -t ";" -n -k 6 academy_nominees.txt | tail
  sort -t ";" -n -k 6 -r academy_nominees.txt | head

- most wins
  sort -t ";" -n -k 7 academy_nominees.txt | tail
  sort -t ";" -n -k 7 -r academy_nominees.txt | head


# file system

# relational

# nosql
## key-value
## wide column store
## document
## graph
## search engine
